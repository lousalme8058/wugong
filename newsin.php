<!DOCTYPE html>	
<head>
<title>蜈蚣社區首頁</title>
<?php require('head.php') ?>
<!-- 首頁輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        loop: true,
        dots: false,
        responsive: {
        320: {
            items: 1
        },
        }
    })
})
</script> -->
</head>
<body>
    <!-- loading動畫 -->
    <!-- <div class="js-patLoadingAniBg patLoading">
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
    </div> -->


    <?php require('header.php') ?>

    <!-- 首頁banner -->
    <article class="patBannerArea">
        <div class="patBannerArea-layer wow"></div>
        <!-- 消息封面照片 -->
        <img src="images/newsli002.jpg" alt="banner" class="patBannerArea-Banner wow">
        <!-- <img src="images/no-image-news.svg" alt="照片" class="patBannerArea-Banner"> -->
    </article>
    
    <div class="eleh1TitBg">
        <div class="eleh1TitArea">
            <h1>鳥類生態課程</h1>
        </div>
    </div>

     <!-- 分類及麵包屑 -->
     <div class="eleClassifyBitesBk">
        <!-- <article class="eleClassify js-eleClassifyBt">
            <div class="eleClassify-tit">Classify</div>
            <div class="eleClassify-selBt plr-20">
                選擇文章分類
                <span class="icon">▼</span>
            </div>
        </article>
        <section class="eleClassify-sel js-eleClassifySel">
            <a href="newsli.php" class="eleClassify-sel--link plr-30">最新消息</a>
            <a href="newsli.php" class="eleClassify-sel--link plr-30">最新消息</a>
            <a href="newsli.php" class="eleClassify-sel--link plr-30">最新消息</a>
        </section> -->
        <article class="eleBite">
            <a href="index.php" class="eleBite-link">
                首頁<span class="icon"> ▸ </span>
            </a>
            <a href="newsli.php" class="eleBite-link">
                社區消息<span class="icon"> ▸ </span>
            </a>
            <a href="newsli.php" class="eleBite-link">
                最新消息<span class="icon"> ▸ </span>
            </a>
            <a href="newsli.php" class="eleBite-link">
                鳥類生態課程<span class="icon"> ▸ </span>
            </a>
        </article>
    </div>

    <!-- 頁面內容 -->
    <div class="patPagePrimaryContent">
        <h6 class="pageNewsinDate">109.10.29</h6>
        <div class="patPrimaryArticleWidth mt-60">
            <!-- 文編塞以下div -->
            <div>
                <!-- 文編內容 -->
                <!-- <img src="images/newsli002.jpg" alt="鳥類生態課程" class="textImg">
                <p class="textImgNote">鳥類生態課程</p>
                <p class="textArticleParagraph">
                    今天是室外賞鳥課~蔡老師、薛老師及喻老師帶大家到石墩坑賞鳥🦅🕊🐦總共發現26種，但實際親眼看到的很少，因為鳥兒怕我們😂<br /><br />
                    老師教大家聽鳥鳴認鳥，因為牠不會明顯的出現在眼前~看到灰喉山椒鳥很興奮，沒多久就飛走了😂接觸大自然，對身體有益唷！老師說聽鳥鳴對身體也有益（每種聲音可對到身體部位），歡迎大家一起來蜈蚣里賞鳥賞蝶顧身體😁
                </p> -->

                <img src="images/newsli002.jpg" alt="圖片欄位" class="textImg">
                <p class="textImgNote">圖片說明</p>
                <p class="textArticleTit">文章段落標題</p>
                <p class="textArticleParagraph">
                    文章內容
                </p>
                <p class="textArticleTit">文章段落標題</p>
                <p class="textArticleParagraph">
                    文章內容
                </p>
                
            </div>
        </div>

        <div class="patPrimaryArticleWidth mt-40 mb-80">
            <a href="javascript:void(0);" class="eleDownloadBt">
                <span class="eleDownloadBt-tit">檔案下載</span>
                蜈蚣社區報第19期.pdf
            </a>
            <a href="javascript:void(0);" class="eleDownloadBt">
                <span class="eleDownloadBt-tit">檔案下載</span>
                蜈蚣社區報第19期.pdf
            </a>
        </div>

        <div class="patPrimaryArticleWidth mtb-60">
            <div class="patPrimarySocialArea ml-50 pb-40">
                <!-- 社群分享放這邊 -->
            </div>
            <a href="newsli.php" class="btnRedBt floatRight mr-50">回上頁</a>
        </div>
    </div>
    
    <?php require('footer.php') ?>

    
</body>
</html>

     