<!DOCTYPE html>	
<head>
<title>蜈蚣社區首頁</title>
<?php require('head.php') ?>
<!-- 首頁輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        loop: true,
        dots: false,
        responsive: {
        320: {
            items: 1
        },
        }
    })
})
</script> -->
</head>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
    </div>


    <?php require('header.php') ?>

    <!-- 首頁banner -->
    <article class="patBannerArea">
        <div class="patBannerArea-layer wow"></div>
        <!-- <div class="patBannerArea-Banner wow"></div> -->
        <img src="images/banner03.png" alt="banner" class="patBannerArea-Banner wow">
        <div class="patBannerArea-titArea">
            <div class="max_width">
                <h2 class="patBannerArea-tit wow">Activities</h2>
            </div>
            <div class="eleh1TitBg">
                <div class="eleh1TitArea">
                    <h1>社區消息：<br />最新消息</h1>
                </div>
            </div>
        </div>
    </article>
    

    <!-- 分類及麵包屑 -->
    <div class="eleClassifyBitesBk">
        <!-- <article class="eleClassify js-eleClassifyBt">
            <div class="eleClassify-tit">Classify</div>
            <div class="eleClassify-selBt plr-20">
                選擇文章分類
                <span class="icon">▼</span>
            </div>
        </article>
        <section class="eleClassify-sel js-eleClassifySel">
            <a href="newsli.php" class="eleClassify-sel--link plr-30">最新消息</a>
            <a href="newsli.php" class="eleClassify-sel--link plr-30">最新消息</a>
            <a href="newsli.php" class="eleClassify-sel--link plr-30">最新消息</a>
        </section> -->
        <article class="eleBite">
            <a href="index.php" class="eleBite-link">
                首頁<span class="icon"> ▸ </span>
            </a>
            <a href="newsli.php" class="eleBite-link">
                社區消息<span class="icon"> ▸ </span>
            </a>
            <a href="newsli.php" class="eleBite-link">
                最新消息<span class="icon"> ▸ </span>
            </a>
        </article>
    </div>

    <!-- 列表 -->
    <article class="patPagePrimaryList pageNewsliPrimary">
        <!-- 一個消息 一頁放12個 -->
        <article class="modNewsli pageNewsli wow animate__animated animate__fadeIn">
            <a href="newsin.php" title="社區消息">  
                <h6 class="modNewsli-classify">社區消息 ▸ 部落格</h6>
                <h5 class="modNewsli-tit mt-40">鳥類生態課程</h5>
                <h6 class="modNewsli-date">2020/10/08</h6>
                <!-- 所有列表圖片建議尺寸 1920*1250 -->
                <img src="images/no-image-news.svg" alt="照片" class="modNewsli-img">
            </a>
        </article>
        <article class="modNewsli pageNewsli wow animate__animated animate__fadeIn">
            <a href="newsin.php" title="社區消息">  
                <h6 class="modNewsli-classify">社區消息 ▸ 部落格</h6>
                <h5 class="modNewsli-tit mt-40">鳥類生態課程</h5>
                <h6 class="modNewsli-date">2020/10/08</h6>
                <!-- 所有列表圖片建議尺寸 1920*1250 -->
                <img src="images/newsli002.jpg" alt="照片" class="modNewsli-img">
            </a>
        </article>
        <article class="modNewsli pageNewsli wow animate__animated animate__fadeIn">
            <a href="newsin.php" title="社區消息">  
                <h6 class="modNewsli-classify">社區消息 ▸ 部落格</h6>
                <h5 class="modNewsli-tit mt-40">鳥類生態課程</h5>
                <h6 class="modNewsli-date">2020/10/08</h6>
                <!-- 所有列表圖片建議尺寸 1920*1250 -->
                <img src="images/newsli003.jpg" alt="照片" class="modNewsli-img">
            </a>
        </article>
        <article class="modNewsli pageNewsli wow animate__animated animate__fadeIn">
            <a href="newsin.php" title="社區消息">  
                <h6 class="modNewsli-classify">社區消息 ▸ 部落格</h6>
                <h5 class="modNewsli-tit mt-40">鳥類生態課程</h5>
                <h6 class="modNewsli-date">2020/10/08</h6>
                <!-- 所有列表圖片建議尺寸 1920*1250 -->
                <img src="images/newsli004.jpg" alt="照片" class="modNewsli-img">
            </a>
        </article>


    </article>


    <!-- 頁數 -->
    <div class="eleSelPageBk floatRight">
        <button class="eleSelPageArrow eleSelPageArrow--left "></button>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount eleSelPageCount--pageIn mlr-5">1</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-5">2</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-5">3</a>
        <button class="eleSelPageArrow eleSelPageArrow--right"></button>
    </div>


    

    

   
    
    <?php require('footer.php') ?>
	
        


    
</body>
</html>

     