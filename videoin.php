<!DOCTYPE html>	
<head>
<title>蜈蚣社區首頁</title>
<?php require('head.php') ?>
<!-- 首頁輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        loop: true,
        dots: false,
        responsive: {
        320: {
            items: 1
        },
        }
    })
})
</script> -->
</script>
</head>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
    </div>

    <?php require('header.php') ?>

    <!-- youtube 用崁入方式的時候須寫該ID -->
    <!-- <div id="ytplayer"></div> -->
    
    <!-- 影片 -->
    <div class="eleVideoBk">
        <iframe src="https://www.youtube.com/embed/VFgoxYvMi3g?&autoplay=1&mute=1" frameborder="0" allowfullscreen  allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
    </div>
    <div class="eleh1TitBg">
        <div class="eleh1TitArea">
            <h1>銅鑼</h1>
        </div>
    </div>

    <!-- 分類及麵包屑 -->
    <div class="eleClassifyBitesBk">
        <article class="eleClassify js-eleClassifyBt">
            <div class="eleClassify-tit">Classify</div>
            <div class="eleClassify-selBt plr-20">
                選擇分類
                <span class="icon">▼</span>
            </div>
        </article>
        <section class="eleClassify-sel js-eleClassifySel">
            <a href="videoli.php" class="eleClassify-sel--link plr-30">文物專訪</a>
            <a href="videoli.php" class="eleClassify-sel--link plr-30">文物專訪</a>
            <a href="videoli.php" class="eleClassify-sel--link plr-30">文物專訪</a>
        </section>
        <article class="eleBite">
            <a href="index.php" class="eleBite-link">
                首頁<span class="icon"> ▸ </span>
            </a>
            <a href="videoli.php" class="eleBite-link">
                影音典藏<span class="icon"> ▸ </span>
            </a>
            <a href="videoli.php" class="eleBite-link">
                文物專訪<span class="icon"> ▸ </span>
            </a>
        </article>
    </div>

    <!-- 頁面內容 -->
    <div class="patPagePrimaryContent">
        <div class="patPrimaryArticleWidth mtb-50">
            <p class="textImgNote">影音簡介</p>
            <!-- 影音簡介 -->
            <p class="textArticleParagraph">
                此為一組棒槌和銅鑼，棒槌為木製，有上漆，箭矢造型，中段有一護手。
                用以敲擊的棒槌頭部有紗布包裹，棒身有麻繩纏繞。

                銅鑼為銅製，圓形，兩個穿孔可繫提繩。
            </p>
        </div>

        <div class="patPrimaryArticleWidth mtb-60">
            <a href="videoli.php" class="btnRedBt floatRight mr-50">回上頁</a>
        </div>
    </div>
    
    <?php require('footer.php') ?>
    <!-- <script>
        // Load the IFrame Player API code asynchronously.
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/player_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        // Replace the 'ytplayer' element with an <iframe> and
        // YouTube player after the API code downloads.

        var player;
        function onYouTubePlayerAPIReady() {
            player = new YT.Player('ytplayer', {
            width: '100%',
            height: '768',
            videoId: 'M7lc1UVf-VE',
            playerVars: {
                controls: 1,
                // showinfo棄用
                // showinfo: 0,
                // // rel: 0,只推薦本頻道影片，rel: 1，也推薦其他頻道影片 
                // rel: 0,
                // modestbranding: 1,
                autoplay: 1,
                mute: 1
            },
            events: {
                //靜音效果
                onReady: function(e) {
                    console.log(e.target);
                    // e.target.mute();
                }
            }
            });
            
        }
    </script> -->


    
</body>
</html>

     