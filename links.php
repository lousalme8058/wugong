<!DOCTYPE html>	
<head>
<title>蜈蚣社區首頁</title>
<?php require('head.php') ?>
<!-- 首頁輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        loop: true,
        dots: false,
        responsive: {
        320: {
            items: 1
        },
        }
    })
})
</script> -->
</head>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
    </div>


    <?php require('header.php') ?>

    <!-- 首頁banner -->
    <article class="patBannerArea">
        <div class="patBannerArea-layer wow"></div>
        <!-- <div class="patBannerArea-Banner wow"></div> -->
        <img src="images/banner07.png" alt="banner" class="patBannerArea-Banner wow">
        <div class="patBannerArea-titArea">
            <div class="max_width">
                <h2 class="patBannerArea-tit wow">Links</h2>
            </div>
            <div class="eleh1TitBg">
                <div class="eleh1TitArea">
                    <h1>相關連結</h1>
                </div>
            </div>
        </div>
    </article>
   

    <!-- 分類及麵包屑 -->
    <div class="eleClassifyBitesBk">
        <article class="eleBite">
            <a href="index.php" class="eleBite-link">
                首頁<span class="icon"> ▸ </span>
            </a>
            <a href="links.php" class="eleBite-link">
                相關連結<span class="icon"> ▸ </span>
            </a>
        </article>
    </div>

    <!-- 列表 -->
    <article class="patPagePrimaryList pageNewsliPrimary">
        <!-- 一個連結 一頁放16個 -->
        <article class="modLinkli wow animate__animated animate__fadeIn">
            <a href="https://www.edu.tw/" title="教育部全球資訊網" target="_blank">  
                <h5 class="modLinkli-tit">教育部全球資訊網</h5>
            </a>
        </article>
        <article class="modLinkli wow animate__animated animate__fadeIn">
            <a href="https://www.edu.tw/" title="教育部全球資訊網" target="_blank">  
                <h5 class="modLinkli-tit">教育部全球資訊網</h5>
            </a>
        </article>
        <article class="modLinkli wow animate__animated animate__fadeIn">
            <a href="https://www.edu.tw/" title="教育部全球資訊網" target="_blank">  
                <h5 class="modLinkli-tit">教育部全球資訊網</h5>
            </a>
        </article>
        <article class="modLinkli wow animate__animated animate__fadeIn">
            <a href="https://www.edu.tw/" title="教育部全球資訊網" target="_blank">  
                <h5 class="modLinkli-tit">教育部全球資訊網</h5>
            </a>
        </article>
    </article>


    <!-- 頁數 -->
    <div class="eleSelPageBk floatRight">
        <button class="eleSelPageArrow eleSelPageArrow--left "></button>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount eleSelPageCount--pageIn mlr-5">1</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-5">2</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-5">3</a>
        <button class="eleSelPageArrow eleSelPageArrow--right"></button>
    </div>


    

    

   
    
    <?php require('footer.php') ?>
	
        


    
</body>
</html>

     