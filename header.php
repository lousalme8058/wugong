<div class="patheaderBk js-patheader">
	<div class="max_width">
		<a href="index.php" class="patLogo js-headerLogo" title="蜈蚣社區數位典藏網首頁">
			<img src="images/logo-white.svg" alt="蜈蚣社區LOGO" class="patLogo-img">
			<p class="patLogo-name">WUGONG</p>
		</a>
		<button class="patheaderBt js-navOpenBt" aria-label="打開網站連結選單">
			<img src="images/nav-open-bt.svg" alt="icon" class="patheaderBt-icon">
		</button>
	</div>
</div>

<div class="patnavBk js-patnavBk">
	<div class="patnav js-patnavContent">
		<div class="max_width">
			
			<div class="patnav-head">
				<a href="index.php" title="回首頁">
					<img src="images/logo.svg" alt="蜈蚣社區LOGO" class="patnav-head--logoImg">
					<p class="patnav-head--logoName">WUGONG</p>
				</a>
				<button class="patnav-head--navCloseBt js-navCloseBt" aria-label="關閉網站連結選單">
					<img src="images/nav-close-bt.svg" alt="icon" class="patnav-head--navCloseBt--icon">
				</button>
			</div>

			<div class="patnavSection">
				<section class="patnavFirstArea js-navFirstLink">
					<h6 class="patnavFirstArea-tit">
						<img src="images/nav-icon001.svg" alt="icon" class="patnavFirstArea-tit--icon">影音典藏
					</h6>
					<section class="patnavSecondArea js-navSecondLink">
						<li class="patnavSecondArea-li"><a href="videoli.php" class="patnavSecondArea-link patnavSecondArea-link--actice">活動紀錄</a></li>
						<li class="patnavSecondArea-li"><a href="videoli.php" class="patnavSecondArea-link">文化紀錄</a></li>
						<li class="patnavSecondArea-li"><a href="videoli.php" class="patnavSecondArea-link">文物專訪</a></li>
						<li class="patnavSecondArea-li"><a href="videoli.php" class="patnavSecondArea-link">推廣創作</a></li>
					</section>
				</section>
			</div>
			<div class="patnavSection">
				<section class="patnavFirstArea js-navFirstLink">
					<h6 class="patnavFirstArea-tit">
						<img src="images/nav-icon002.svg" alt="icon" class="patnavFirstArea-tit--icon">人文典藏
					</h6>
					<section class="patnavSecondArea js-navSecondLink">
						<li class="patnavSecondArea-li"><a href="cultureli.php" class="patnavSecondArea-link">物料工具裝備</a></li>
						<li class="patnavSecondArea-li"><a href="cultureli.php" class="patnavSecondArea-link">個人物及飾品</a></li>
						<li class="patnavSecondArea-li"><a href="cultureli.php" class="patnavSecondArea-link">儀式或娛樂</a></li>
						<li class="patnavSecondArea-li"><a href="cultureli.php" class="patnavSecondArea-link">社會政治器物</a></li>
						<li class="patnavSecondArea-li"><a href="cultureli.php" class="patnavSecondArea-link">交通與運送</a></li>
						<li class="patnavSecondArea-li"><a href="cultureli.php" class="patnavSecondArea-link">影音媒材</a></li>
					</section>
				</section>
			</div>
			<div class="patnavSection">
				<section class="patnavFirstArea js-navFirstLink">
					<h6 class="patnavFirstArea-tit">
						<img src="images/nav-icon003.svg" alt="icon" class="patnavFirstArea-tit--icon">自然典藏
					</h6>
					<section class="patnavSecondArea js-navSecondLink">
						<li class="patnavSecondArea-li"><a href="naturalli.php" class="patnavSecondArea-link">陸生</a></li>
						<li class="patnavSecondArea-li"><a href="naturalli.php" class="patnavSecondArea-link">水生</a></li>
						<li class="patnavSecondArea-li"><a href="naturalli.php" class="patnavSecondArea-link">兩生</a></li>
						<li class="patnavSecondArea-li"><a href="naturalli.php" class="patnavSecondArea-link">禽鳥</a></li>
						<li class="patnavSecondArea-li"><a href="naturalli.php" class="patnavSecondArea-link">蟲</a></li>
						<li class="patnavSecondArea-li"><a href="naturalli.php" class="patnavSecondArea-link">植物菌藻</a></li>
						<li class="patnavSecondArea-li"><a href="naturalli.php" class="patnavSecondArea-link">礦物</a></li>
					</section>
				</section>
			</div>
			<div class="patnavSection">
				<section class="patnavFirstArea patnavFirstArea--noIcon js-navFirstLink">
					<h6 class="patnavFirstArea-tit">
						村落介紹
					</h6>
					<section class="patnavSecondArea patnavSecondArea--noIcon js-navSecondLink">
						<li class="patnavSecondArea-li"><a href="aboutli.php" class="patnavSecondArea-link">關於我們</a></li>
						<li class="patnavSecondArea-li"><a href="aboutli.php" class="patnavSecondArea-link">社區組織</a></li>
						<li class="patnavSecondArea-li"><a href="aboutli.php" class="patnavSecondArea-link">村落地景</a></li>
					</section>
				</section>
				<section class="patnavFirstArea patnavFirstArea--noIcon js-navFirstLink">
					<h6 class="patnavFirstArea-tit">
						社區消息
					</h6>
					<section class="patnavSecondArea patnavSecondArea--noIcon js-navSecondLink">
						<li class="patnavSecondArea-li"><a href="newsli.php" class="patnavSecondArea-link">最新消息</a></li>
						<li class="patnavSecondArea-li"><a href="newsli.php" class="patnavSecondArea-link">部落格</a></li>
						<li class="patnavSecondArea-li"><a href="newsli.php" class="patnavSecondArea-link">相關連結</a></li>
					</section>
				</section>
				<section class="patnavFirstArea patnavFirstArea--noIcon">
					<li class="patnavSecondArea-li patnavSecondArea--noIcon"><a href="links.php" class="patnavSecondArea-link">相關連結</a></li>
					<br>
					<br>
					<a href="javascript:void(0);" class="patnavFirstArea-tit">
						<img src="images/nav-icon004.svg" alt="" class="" width="40" height="40">
					</a>
					<br>
					<br>
				</section>	
			</div>
		</div>
	</div>
	<div class="">
		<div class="patnavBg js-navBg"></div>
		<div class="patnavBg js-navBg"></div>
		<div class="patnavBg js-navBg"></div>
		<div class="patnavBg js-navBg"></div>
	</div>

</div>