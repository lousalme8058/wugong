<footer class="patFooter">
	<div class="max_width plr-20 overflow-hidden">
		<div class="patGrid25">
			<article class="patFooter-logoArea">
				<img src="images/logo.svg" alt="logo" class="patFooter-logoArea--logo">
				<p class="patFooter-logoArea--enName">WUGONG</p>
				<p class="patFooter-logoArea--chName">蜈蚣社區數位典藏網</p>
				<p class="patFooter-logoArea--copyright mt-80">© 2020 Wugong 545</p>
			</article>
		</div>
		<div class="patGrid25 patFooter-linkArea">
			<p class="patFooter--tit mb-30 pl-5">Wugong 545 relics</p>
			<a href="videoli.php" class="patFooter--link">影音典藏</a><br />
			<a href="cultureli.php" class="patFooter--link">人文典藏</a><br />
			<a href="naturalli.php" class="patFooter--link">自然典藏</a><br />
		</div>
		<div class="patGrid25 patFooter-linkArea">
			<p class="patFooter--tit mb-30 pl-5">About Wugong 545</p>
			<a href="aboutli.php" class="patFooter--link">村落介紹</a><br />
			<a href="newsli.php" class="patFooter--link">社區消息</a><br />
			<a href="links.php" class="patFooter--link">連絡我們</a><br />
		</div>
		<div class="patGrid25 patFooter-linkArea">
			<p class="patFooter--tit mb-30 pl-5">Infomation</p>
			<p class="patFooter--text pl-5">
				04 - 92992540 <br />
				545　南投縣埔里鎮蜈蚣路36號<br />
				wugon107@gmail.com
			</p>
		</div>
	</div>
	<div class="patFooter-unitArea">
		<article class="patFooter-unitArea--gov">
			補助單位：原住民族委員會<br />
			執行單位：南投縣埔里鎮蜈蚣社區發展協會<br />
			計畫別：109年平埔族群聚落活力計畫「與噶哈巫相遇在雲端」
		</article>
		<article class="patFooter-unitArea--contract">
			企劃指導：<a href="" class="">樂悠遊有限公司</a><br />
			網頁設計：<a href="https://cosmosdesign.tw/" class="">小宇宙資訊有限公司</a><br />
			文物攝影、影片製作：<a href="https://www.facebook.com/yoingmaking" class="">優影設計</a>
		</article>
	</div>
</footer>