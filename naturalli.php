<!DOCTYPE html>	
<head>
<title>蜈蚣社區首頁</title>
<?php require('head.php') ?>
<!-- 首頁輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        loop: true,
        dots: false,
        responsive: {
        320: {
            items: 1
        },
        }
    })
})
</script> -->
</head>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
    </div>


    <?php require('header.php') ?>

    <!-- 首頁banner -->
    <article class="patBannerArea">
        <div class="patBannerArea-layer wow"></div>
        <img src="images/banner06.png" alt="banner" class="patBannerArea-Banner wow">
        <div class="patBannerArea-titArea">
            <div class="max_width">
                <h2 class="patBannerArea-tit wow">Ecology</h2>
            </div>
            <div class="eleh1TitBg">
                <div class="eleh1TitArea">
                    <h1>自然典藏：<br />蝴蝶</h1>
                </div>
            </div>
        </div>
    </article>
    

    <!-- 分類及麵包屑 -->
    <div class="eleClassifyBitesBk">
        <!-- <article class="eleClassify js-eleClassifyBt">
            <div class="eleClassify-tit">Classify</div>
            <div class="eleClassify-selBt plr-20">
                選擇文章分類
                <span class="icon">▼</span>
            </div>
        </article>
        <section class="eleClassify-sel js-eleClassifySel">
            <a href="naturalli.php" class="eleClassify-sel--link plr-30">蝴蝶</a>
            <a href="naturalli.php" class="eleClassify-sel--link plr-30">蝴蝶</a>
            <a href="naturalli.php" class="eleClassify-sel--link plr-30">蝴蝶</a>
        </section> -->
        <article class="eleBite">
            <a href="index.php" class="eleBite-link">
                首頁<span class="icon"> ▸ </span>
            </a>
            <a href="naturalli.php" class="eleBite-link">
                自然典藏：蝴蝶<span class="icon"> ▸ </span>
            </a>
        </article>
    </div>
    <div class="clear"></div>


    <!-- 列表 -->
    <article class="patPagePrimaryList">
        <!-- 一頁放8個 -->
        <!-- 一個文章連結 -->
        <li class="modArticleli wow animate__animated animate__fadeIn">
            <a href="naturalin.php" title="楓香公園">
                <div class="eleImgBk">
                    <!-- 所有列表圖片建議尺寸 1920*1250 -->
                    <img src="images/textImg003.png" alt="大鳳蝶" class=" eleImgBk-img modArticleli-img">
                </div>
                <h5 class="modArticleli-tit mt-40 mb-25">大鳳蝶</h5>
                <div class="modArticleli-text">
                    －從埔里蜈蚣社區出發，紀錄蜈蚣崙豐富的自然生態、人文歷史，以及Kaxabu － 噶哈巫族的部落文化及生活軌跡。
                </div>
            </a>
        </li>
        <li class="modArticleli wow animate__animated animate__fadeIn">
            <a href="naturalin.php" title="楓香公園">
                <div class="eleImgBk">
                    <!-- 所有列表圖片建議尺寸 1920*1250 -->
                    <img src="images/textImg003.png" alt="大鳳蝶" class=" eleImgBk-img modArticleli-img">
                </div>
                <h5 class="modArticleli-tit mt-40 mb-25">大鳳蝶</h5>
                <div class="modArticleli-text">
                    －從埔里蜈蚣社區出發，紀錄蜈蚣崙豐富的自然生態、人文歷史，以及Kaxabu － 噶哈巫族的部落文化及生活軌跡。
                </div>
            </a>
        </li>
        <li class="modArticleli wow animate__animated animate__fadeIn">
            <a href="naturalin.php" title="楓香公園">
                <div class="eleImgBk">
                    <!-- 所有列表圖片建議尺寸 1920*1250 -->
                    <img src="images/textImg003.png" alt="大鳳蝶" class=" eleImgBk-img modArticleli-img">
                </div>
                <h5 class="modArticleli-tit mt-40 mb-25">大鳳蝶</h5>
                <div class="modArticleli-text">
                    －從埔里蜈蚣社區出發，紀錄蜈蚣崙豐富的自然生態、人文歷史，以及Kaxabu － 噶哈巫族的部落文化及生活軌跡。
                </div>
            </a>
        </li>
        <li class="modArticleli wow animate__animated animate__fadeIn">
            <a href="naturalin.php" title="楓香公園">
                <div class="eleImgBk">
                    <!-- 所有列表圖片建議尺寸 1920*1250 -->
                    <img src="images/textImg003.png" alt="大鳳蝶" class=" eleImgBk-img modArticleli-img">
                </div>
                <h5 class="modArticleli-tit mt-40 mb-25">大鳳蝶</h5>
                <div class="modArticleli-text">
                    －從埔里蜈蚣社區出發，紀錄蜈蚣崙豐富的自然生態、人文歷史，以及Kaxabu － 噶哈巫族的部落文化及生活軌跡。
                </div>
            </a>
        </li>
    </article>

    <div class="eleSelPageBk">
        <button class="eleSelPageArrow eleSelPageArrow--left "></button>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount eleSelPageCount--pageIn mlr-5">1</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-5">2</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-5">3</a>
        <button class="eleSelPageArrow eleSelPageArrow--right"></button>
    </div>
    
    

    

   
    
    <?php require('footer.php') ?>
	
        


    
</body>
</html>

     