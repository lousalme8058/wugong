<!DOCTYPE html>	
<head>
<title>蜈蚣社區首頁</title>
<?php require('head.php') ?>
<!-- 首頁輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        loop: true,
        dots: false,
        responsive: {
        320: {
            items: 1
        },
        }
    })
})
</script> -->
</head>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
    </div>

    <?php require('header.php') ?>

    <!-- 首頁banner -->
    <article class="patBannerArea">
        <div class="patBannerArea-layer wow"></div>
        <img src="images/banner05.png" alt="banner" class="patBannerArea-Banner wow">
        <div class="patBannerArea-titArea">
            <div class="max_width">
                <h2 class="patBannerArea-tit wow">Cultural</h2>
            </div>
            <div class="eleh1TitBg">
                <div class="eleh1TitArea">
                    <h1>人文典藏：<br />物料工具裝備</h1>
                </div>
            </div>
        </div>
    </article>
    

    <!-- 分類及麵包屑 -->
    <div class="eleClassifyBitesBk">
        <article class="eleClassify js-eleClassifyBt">
            <div class="eleClassify-tit">Classify</div>
            <div class="eleClassify-selBt plr-20">
                選擇分類
                <span class="icon">▼</span>
            </div>
        </article>
        <section class="eleClassify-sel js-eleClassifySel">
            <a href="videoli.php" class="eleClassify-sel--link plr-30">物料工具裝備</a>
            <a href="videoli.php" class="eleClassify-sel--link plr-30">物料工具裝備</a>
            <a href="videoli.php" class="eleClassify-sel--link plr-30">物料工具裝備</a>
        </section>
        <article class="eleBite">
            <a href="index.php" class="eleBite-link">
                首頁<span class="icon"> ▸ </span>
            </a>
            <a href="videoli.php" class="eleBite-link">
                人文典藏<span class="icon"> ▸ </span>
            </a>
            <a href="videoli.php" class="eleBite-link">
                物料工具裝備<span class="icon"> ▸ </span>
            </a>
        </article>
    </div>
    <div class="clear"></div>

    <!-- 列表 -->
    <article class="patPagePrimaryList pageCultureliPrimary">
        <!-- 一頁放12個 -->
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/no-image.svg" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/no-image.svg" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/no-image.svg" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/no-image.svg" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/video-img001.png" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/video-img001.png" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/video-img001.png" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/video-img001.png" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/video-img001.png" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/video-img001.png" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/video-img001.png" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
        <article class="modRelicsli pageCultureList wow animate__animated animate__fadeInLeft">
            <a href="culturein.php" title="文物典藏影片">
                <div class="eleImgBk">
                    <img src="images/video-img001.png" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                </div>
                <div class="modRelicsli-titArea pageCultureList-titArea">
                    <h6 class="modRelicsli-titArea--classify pageCultureList-titArea--classify">農林漁牧用具 ▸ 農林漁牧用具</h6>
                    <h5 class="modRelicsli-titArea--name pageCultureList-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                </div>
            </a>
        </article>
    </article>

    <div class="eleSelPageBk">
        <button class="eleSelPageArrow eleSelPageArrow--left "></button>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount eleSelPageCount--pageIn mlr-5">1</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-5">2</a>
        <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-5">3</a>
        <button class="eleSelPageArrow eleSelPageArrow--right"></button>
    </div>
    
    

    

   
    
    <?php require('footer.php') ?>
	
        


    
</body>
</html>

     