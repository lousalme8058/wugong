<!DOCTYPE html>	
<head>
<title>蜈蚣社區首頁</title>
<?php require('head.php') ?>
<!-- 輪播 -->
<link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        loop: true,
        dots: false,
        responsive: {
        320: {
            items: 1
        },
        }
    })
})
</script>
</head>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
    </div>

    <?php require('header.php') ?>

    <!-- 首頁banner -->
    <div class="eleVideoBk">
        <iframe src="https://www.youtube.com/embed/VFgoxYvMi3g?&autoplay=1&mute=1" frameborder="0" allowfullscreen  allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
    </div>

    <div class="eleh1TitBg">
        <div class="eleh1TitArea">
            <h1>銅鑼及銅鑼槌</h1>
        </div>
    </div>

    <!-- 分類及麵包屑 -->
    <div class="eleClassifyBitesBk">
        <article class="eleClassify js-eleClassifyBt">
            <div class="eleClassify-tit">Classify</div>
            <div class="eleClassify-selBt plr-20">
                選擇分類
                <span class="icon">▼</span>
            </div>
        </article>
        <section class="eleClassify-sel js-eleClassifySel">
            <a href="videoli.php" class="eleClassify-sel--link plr-30">物料工具裝備</a>
            <a href="videoli.php" class="eleClassify-sel--link plr-30">物料工具裝備</a>
            <a href="videoli.php" class="eleClassify-sel--link plr-30">物料工具裝備</a>
        </section>
        <article class="eleBite">
            <a href="index.php" class="eleBite-link">
                首頁<span class="icon"> ▸ </span>
            </a>
            <a href="videoli.php" class="eleBite-link">
                人文典藏<span class="icon"> ▸ </span>
            </a>
            <a href="videoli.php" class="eleBite-link">
                物料工具裝備 ▸ 物料工具裝備<span class="icon"> ▸ </span>
            </a>
            <a href="videoli.php" class="eleBite-link">
                銅鑼及銅鑼槌<span class="icon"> ▸ </span>
            </a>
        </article>
    </div>

    <!-- 照片輪播區 -->
    <div class="pageCulinSectionArea">
        <div class="pageCulinSectionArea-titArea">
            <section class="eleSecTitArea plr-20">
                <h3 class="eleSecTitEn">Photos</h3>
                <h4 class="eleSecTitCh">
                    文物照片
                </h4>
            </section>
        </div>
        <div class="pageCulinSectionArea-content">
            <div class="owl-carousel">
                <!-- 一張照片 -->
                <div class="overflow-hidden">
                    <img src="images/video-img001.png" alt="">
                    <p class="textImgNote pageCulinSectionArea-note">
                        棒槌：寬（棒身2.8、護手6、尾端6.5）；長34.5；厚2.5
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- 頁面內容 -->
    <div class="patPagePrimaryContent">
        <div class="patPrimaryArticleWidth mtb-50">
            <p class="textImgNote">基本資料</p>
            <!-- 影音簡介 -->
            <p class="textArticleParagraph">
                來源：蜈蚣社區美琴<br />
                棒槌：寬（棒身2.8、護手6、尾端6.5）；長34.5；厚2.5 <br>
                銅鑼：直徑40.5；厚2
            </p>
            
            <div>
                <!-- 文編塞這裡 -->
                <p class="textArticleParagraph">
                    1. 簡介：以前社會沒有塑膠製或其他材質製作的水瓢，故將曬乾後的瓠瓜當作水瓢使用。<br />
                    2. 形制：為葫蘆形狀，內有凹槽將水裝在裡面。<br />
                    3. 製作方式：將品項適合的瓠瓜切開剖半，刮除內部的瓜肉並將內部的厚度修到平均一致，將瓠瓜水瓢曬乾後變硬即可舀水使用。<br />
                    4. 功能：用來舀水日常生活用具
                </p>
            </div>
        </div>
    </div>

    <!-- 相關影片區 -->
    <div class="pageCulinSectionArea mb-50">
        <div class="pageCulinSectionArea-titArea">
            <section class="eleSecTitArea plr-20">
                <h3 class="eleSecTitEn">Videos</h3>
                <h4 class="eleSecTitCh">
                    相關影片
                </h4>
            </section>
        </div>
        <div class="pageCulinSectionArea-content">
            <!-- 一個文章連結 -->
            <li class="pageCulinVideoli wow animate__animated animate__fadeInRight">
                <a href="videoin.php" title="楓香公園">
                    <div class="eleImgBk">
                        <!-- 所有列表圖片建議尺寸 1920*1250 -->
                        <img src="images/listimg001.png" alt="楓香公園" class=" eleImgBk-img pageCulinVideoli-img">
                    </div>
                    <h5 class="pageCulinVideoli-tit mlr-30 mt-40 mb-25">銅鑼</h5>
                    <div class="pageCulinVideoli-text mlr-30 mt-30 mb-30 ">
                        －從埔里蜈蚣社區出發，紀錄蜈蚣崙豐富的自然生態、人文歷史，以及Kaxabu － 噶哈巫族的部落文化及生活軌跡。
                    </div>
                </a>
            </li>
            <li class="pageCulinVideoli wow animate__animated animate__fadeInRight">
                <a href="videoin.php" title="楓香公園">
                    <div class="eleImgBk">
                        <!-- 所有列表圖片建議尺寸 1920*1250 -->
                        <img src="images/listimg002.png" alt="楓香公園" class=" eleImgBk-img pageCulinVideoli-img">
                    </div>
                    <h5 class="pageCulinVideoli-tit mlr-30 mt-40 mb-25">銅鑼</h5>
                    <div class="pageCulinVideoli-text mlr-30 mt-30 mb-30 ">
                        －從埔里蜈蚣社區出發，紀錄蜈蚣崙豐富的自然生態、人文歷史，以及Kaxabu － 噶哈巫族的部落文化及生活軌跡。
                    </div>
                </a>
            </li>
            <li class="pageCulinVideoli wow animate__animated animate__fadeInRight">
                <a href="videoin.php" title="楓香公園">
                    <div class="eleImgBk">
                        <!-- 所有列表圖片建議尺寸 1920*1250 -->
                        <img src="images/listimg003.png" alt="楓香公園" class=" eleImgBk-img pageCulinVideoli-img">
                    </div>
                    <h5 class="pageCulinVideoli-tit mlr-30 mt-40 mb-25">銅鑼</h5>
                    <div class="pageCulinVideoli-text mlr-30 mt-30 mb-30 ">
                        －從埔里蜈蚣社區出發，紀錄蜈蚣崙豐富的自然生態、人文歷史，以及Kaxabu － 噶哈巫族的部落文化及生活軌跡。
                    </div>
                </a>
            </li>
        </div>
    </div>
    <!-- <div class="patPrimaryArticleWidth mtb-60">
        <a href="videoli.php" class="btnRedBt floatRight mr-50">回上頁</a>
    </div>

     -->
   
    
    <?php require('footer.php') ?>
	
        


    
</body>
</html>

     