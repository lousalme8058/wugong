<!DOCTYPE html>	
<head>
<title>蜈蚣社區首頁</title>
<?php require('head.php') ?>
<!-- 首頁輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        loop: true,
        dots: false,
        responsive: {
        320: {
            items: 1
        },
        }
    })
})
</script> -->
<script language="javascript">
$(window).on('load',function(){
    setTimeout(function(){
        if($(window).width() >= 1280){
            // $(window).scrollTop(220);
            // animate屬性要拉動的是body 因為window沒有scroll bar
            $('html,body').animate({scrollTop:220},1200);
        }
    }, 1000);
});
</script>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
    </div>


    <?php require('header.php') ?>

    <!-- 首頁banner -->
    <article class="indBannerArea">
        <div class="indBannerArea-layer wow"></div>
        <div id="selector" class="indBannerArea-Banner wow"></div>
        <div class="indBannerArea-titArea">
            <div class="max_width">
                <h2 class="indBannerArea-tit wow">WUGONG 545</h2>
            </div>
        </div>
    </article>

    <div class="eleh1TitBg">
        <div class="eleh1TitArea">
            <h1>蜈蚣社區<br />數位典藏網</h1>
        </div>
    </div>

    <p class="indIntro">
        <span>
            －從埔里蜈蚣社區出發，紀錄蜈蚣崙豐富的自然生態、人文歷史，以及Kaxabu－噶哈巫族的部落文化及生活軌跡。
        </span>
        <br /><br />
            蜈蚣社區過去為臺中水底寮社的平埔族Kaxabu－噶哈巫，集體移居至埔里並建庄。因山形像一隻往下爬行的蜈蚣，又以地形稱作「蜈蚣崙」。因地理位置關係，為面對高山族群出草威脅的最前線，曾設有清軍的營寨、番丁守隘的望高寮、物資交換的番產交易所等；現為埔里通往霧社的門戶要衝、國道六號的終點站。
    </p>

    <!-- 文物典藏區 -->
    <article class="indRelicsArea">
        <div class="indRelicsArea-tit">
            <section class="eleSecTitArea plr-20">
                <h3 class="eleSecTitEn">Cultural relics</h3>
                <h4 class="eleSecTitCh">
                    <img src="images/nav-icon001.svg" alt="icon" class="eleSecTitCh-icon" >
                    社區文物典藏
                </h4>
            </section>
        </div>
        <div class="indRelicsArea-li">
            <article class="modRelicsli wow animate__animated animate__fadeInRight">
                <a href="videoli.php" title="文物典藏影片">
                    <div class="eleImgBk">
                        <img src="images/no-image.svg" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                    </div>
                    <div class="modRelicsli-titArea">
                        <h6 class="modRelicsli-titArea--classify">物料工具設備 ▸ 農林漁牧用具</h6>
                        <h5 class="modRelicsli-titArea--name mt-5">銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌銅鑼及銅鑼槌</h5>
                    </div>
                </a>
            </article>
            <article class="modRelicsli wow animate__animated animate__fadeInRight">
                <a href="videoli.php" title="文物典藏影片">
                    <div class="eleImgBk">
                        <img src="images/video-img002.png" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                    </div>
                    <div class="modRelicsli-titArea">
                        <h6 class="modRelicsli-titArea--classify">物料工具設備 ▸ 農林漁牧用具</h6>
                        <h5 class="modRelicsli-titArea--name mt-5">刀</h5>
                    </div>
                </a>
            </article>
            <article class="modRelicsli wow animate__animated animate__fadeInRight">
                <a href="videoli.php" title="文物典藏影片">
                    <div class="eleImgBk">
                        <img src="images/video-img003.png" alt="文物典藏照片" class="eleImgBk-img modRelicsli-img">
                    </div>
                    <div class="modRelicsli-titArea">
                        <h6 class="modRelicsli-titArea--classify">物料工具設備 ▸ 農林漁牧用具</h6>
                        <h5 class="modRelicsli-titArea--name mt-5">籤筒及籤</h5>
                    </div>
                </a>
            </article>
        </div>
        <div class="clear"></div>
    </article>

    <!-- 社區消息區 -->
    <article class="indNewsArea">
        <div class="indNewsArea-tit">
            <section class="eleSecTitArea plr-20">
                <h3 class="eleSecTitEn">Activities</h3>
                <h4 class="eleSecTitCh">
                    <img src="images/news-icon.svg" alt="icon" class="eleSecTitCh-icon" >
                    社區消息
                </h4>
            </section>
        </div>
        <div class="indNewsArea-li">
            <article class="modNewsli  wow animate__animated animate__fadeInLeft">
                <a href="newsli.php" title="社區消息">  
                    <h6 class="modNewsli-classify">社區消息 ▸ 部落格</h6>
                    <h5 class="modNewsli-tit mt-40">蜈蚣崙山蝴蝶調查蜈蚣崙山蝴蝶調查蜈蚣崙山蝴蝶調查蜈蚣崙山蝴蝶調查蜈蚣崙山蝴蝶調查蜈蚣崙山蝴蝶調查</h5>
                    <h6 class="modNewsli-date">2020/10/08</h6>
                    <img src="images/no-image-news.svg" alt="照片" class="modNewsli-img">
                </a>
            </article>
            <article class="modNewsli  wow animate__animated animate__fadeInLeft">
                <a href="newsli.php" title="社區消息">  
                    <h6 class="modNewsli-classify">社區消息 ▸ 部落格</h6>
                    <h5 class="modNewsli-tit mt-40">蜈蚣崙山蝴蝶調查</h5>
                    <h6 class="modNewsli-date">2020/10/08</h6>
                    <img src="images/video-img001.png" alt="照片" class="modNewsli-img">
                </a>
            </article>
            <article class="modNewsli  wow animate__animated animate__fadeInLeft">
                <a href="newsli.php" title="社區消息">  
                    <h6 class="modNewsli-classify">社區消息 ▸ 部落格</h6>
                    <h5 class="modNewsli-tit mt-40">蜈蚣崙山蝴蝶調查</h5>
                    <h6 class="modNewsli-date">2020/10/08</h6>
                    <img src="images/video-img001.png" alt="照片" class="modNewsli-img">
                </a>
            </article>
            
        </div>
        <div class="clear"></div>
    </article>

    <!-- google map -->
    <article class="indMapArea">
        <div class="indMapArea-tit">
            <section class="eleSecTitArea plr-20">
                <h3 class="eleSecTitEn">Wugong 545</h3>
                <h4 class="eleSecTitCh">
                    <img src="images/map-icon.svg" alt="icon" class="eleSecTitCh-icon" >
                    歡迎參訪蜈蚣社區
                </h4>
            </section>
        </div>
        <div class="indMapArea-map">
            <!-- <div class="indMapArea-map--layer wow animate__animated animate__fadeOutDownBig"></div> -->
            <!-- google map放這裡 -->
            <img src="images/map.png" width="100%" height="100%" alt="">
        </div>
        <div class="clear"></div>
    </article>

   
    
    <?php require('footer.php') ?>
	
        


    
</body>
</html>

     