<!DOCTYPE html>	
<head>
<title>蜈蚣社區首頁</title>
<?php require('head.php') ?>
<!-- 首頁輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        loop: true,
        dots: false,
        responsive: {
        320: {
            items: 1
        },
        }
    })
})
</script> -->
</head>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
        <div class="patLoading-block js-patLoadingAni"></div>
    </div>


    <?php require('header.php') ?>

    <!-- 首頁banner -->
    <article class="patBannerArea">
        <div class="patBannerArea-layer wow"></div>
        <!-- 消息封面照片 -->
        <img src="images/banner02.png" alt="banner" class="patBannerArea-Banner wow">
        <div class="eleh1TitBg">
            <div class="eleh1TitArea">
                <h1>眉溪</h1>
            </div>
        </div>
    </article>

    <!-- 分類及麵包屑 -->
    <div class="eleClassifyBitesBk">
        <article class="eleClassify js-eleClassifyBt">
            <div class="eleClassify-tit">Classify</div>
            <div class="eleClassify-selBt plr-20">
                選擇文章分類
                <span class="icon">▼</span>
            </div>
        </article>
        <section class="eleClassify-sel js-eleClassifySel">
            <a href="aboutli.php" class="eleClassify-sel--link plr-30">關於我們</a>
            <a href="aboutli.php" class="eleClassify-sel--link plr-30">關於我們</a>
            <a href="aboutli.php" class="eleClassify-sel--link plr-30">關於我們</a>
        </section>
        <article class="eleBite">
            <a href="index.php" class="eleBite-link">
                首頁<span class="icon"> ▸ </span>
            </a>
            <a href="aboutli.php" class="eleBite-link">
                村落介紹<span class="icon"> ▸ </span>
            </a>
            <a href="aboutli.php" class="eleBite-link">
                村落地景<span class="icon"> ▸ </span>
            </a>
            <a href="aboutin.php" class="eleBite-link">
                眉溪<span class="icon"> ▸ </span>
            </a>
        </article>
    </div>

    <!-- 頁面內容 -->
    <div class="patPagePrimaryContent">
        <div class="patPrimaryArticleWidth mtb-50">
            <!-- 文編塞以下div -->
            <div>
                <!-- 文編內容 -->
                <img src="images/textImg002.png" alt="眉溪" class="textImg">
                <p class="textImgNote">108.8 眉溪與蜈蚣崙山－劉慶山拍</p>
                <p class="textArticleParagraph">
                    眉溪（賽德克語：bakei、iyu、sdringan）位於台灣中部，屬於烏溪水系，為南港溪的主流（此處指最長河道）上游，流域面積136.41平方公里，分布於南投縣埔里鎮北部及仁愛鄉中西部。其源流為哈奔溪，發源於北東眼山、三角峰之間的合望鞍部一帶，向西南流經霧社，於人止關附近與北側流來之
                    東眼溪會合，始稱眉溪，續流經楓樹林、獅子頭、九芎林、蜈蚣崙，進入埔里盆地後繞行盆地北部，於盆地西部之向善橋附近注入南港溪。眉溪河谷為通往台灣東部地區的重要孔道之一，省道台14線即通過此一地段。
                </p>
                <p class="textArticleTit">眉溪主要支流</p>
                <p class="textArticleParagraph">
                    眉溪（賽德克語：bakei、iyu、sdringan）位於台灣中部，屬於烏溪水系，為南港溪的主流（此處指最長河道）上游，流域面積136.41平方公里，分布於南投縣埔里鎮北部及仁愛鄉中西部。其源流為哈奔溪，發源於北東眼山、三角峰之間的合望鞍部一帶，向西南流經霧社，於人止關附近與北側流來之
                    東眼溪會合，始稱眉溪，續流經楓樹林、獅子頭、九芎林、蜈蚣崙，進入埔里盆地後繞行盆地北部，於盆地西部之向善橋附近注入南港溪。眉溪河谷為通往台灣東部地區的重要孔道之一，省道台14線即通過此一地段。
                </p>
            </div>
        </div>

        <div class="patPrimaryArticleWidth mtb-60">
            <a href="aboutli.php" class="btnRedBt floatRight mr-50">回上頁</a>
        </div>
    </div>
    
    <?php require('footer.php') ?>

    
</body>
</html>

     