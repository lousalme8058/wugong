/*網站區js*/
$(document).ready(function() {
	

	/*小視口導覽列開合*/
	$(".js-navOpenBt").on('click',function(){
		// console.log("我有觸發click事件");
		//0.6 為整個動作在0.6秒間完成
		TweenMax.to(".js-patheader", 0.3, {ease: Power4.easeInOut, y: -200 });
		TweenMax.to(".js-patnavBk", 0, {zIndex: 5});
		TweenMax.staggerTo(".js-navBg", 0.4, { backgroundColor: "#FFF" ,width:"25%", opacity: 1, delay: 0.5, ease: Power4.inOut, }, 0.15);
		TweenMax.to(".js-patnavContent", 0.5, {zIndex: 7,opacity: 1,delay: 1});
	});
	$(".js-navCloseBt").on('click',function(){
		// console.log("我有觸發關閉事件");
		TweenMax.to(".js-patnavBk", 0.5, {zIndex: -1,delay: 1});
		TweenMax.to(".js-patnavContent", 0.5, {zIndex: 0,opacity: 0,delay: 0.5});
		TweenMax.staggerTo(".js-navBg", 0.4, { backgroundColor: "rgba(250,17,7,1)" ,width:"0%", opacity: 0, delay: 0.5, ease: Power4.inOut, }, 0.15);
		TweenMax.to(".js-patheader", 0.3, {ease: Power4.easeInOut, y: 0 ,delay: 1});
	});


	/* 滑動消失header的logo*/
	$(window).scroll(function() {
		// alert(123);
		if($(window).scrollTop() >= 400){
			// $(".patSmlHeader").show();
			TweenMax.to(".js-headerLogo", 0.2, {ease: Power4.easeInOut, y: -200 });
		}else if($(window).scrollTop()<= 400){
			// $(".patSmlHeader").hide();
			TweenMax.to(".js-headerLogo", 0.2, {ease: Power4.easeInOut, y: 0 });
		}
	});


	/*小視口導覽列以及大視口導覽列第二層開合特效*/
	$(".js-navFirstLink").on('click',function(){
		if($(this).children(".js-navSecondLink").css("display") == "none" ){
			// TweenMax.to(".js-navFirstLink > .js-navSecondLink", 0.5, {display: "none", opacity: 0 ,height:"0" ,ease: Power4.inOut});
			// var $thisSecondLink =  $(this).children(".js-navSecondLink");
			// TweenMax.to( $thisSecondLink, 0.5, {display: "block", opacity: 1 ,height:"auto" ,ease: Power4.inOut});
			$(".js-navFirstLink > .js-navSecondLink").slideUp(300);
            $(this).children(".js-navSecondLink").slideDown(300);
		}else if($(window).width >= 768 && $(this).children(".js-navSecondLink").css("display") == "block" ){
			$(this).children(".js-navSecondLink").slideUp(300);
		}

	});

	//小視口通知按鈕
	$(".jsNoteBt").on('click',function(){
		// console.log("我有觸發click事件");
		$(".jsNavSmall").animate({
		  left: '-320',
		});
		$(".jspatSmlNavNote").animate({
			left: '0',
		});
		$(".jsNavSml-openbg").show();
	});
	$(".jsNavSmlNote-bt--close").on('click',function(){
		// console.log("我有觸發關閉事件");
		$(".jspatSmlNavNote").animate({
			left: '-320',
		});
		$(".jsNavSml-openbg").fadeOut(500);
	});
	

	/*classify 打開select*/
	$(".js-eleClassifyBt").on('click',function(){
		if($(".js-eleClassifySel").css("display") == "none" ){
            $(".js-eleClassifySel").slideDown(300);
		}else if($(".js-eleClassifySel").css("display") == "block" ){
			$(".js-eleClassifySel").slideUp(300);
		}
	});



	// 出現錨點
	// $(".jsindthumbArea").css("display","none");
	// $(window).scroll(function(){
	// 	var bannerHeight = $(".indBanner").height();
	// 	// console.log("banner高度:"+bannerHeight);
	// 	var showBtH = bannerHeight - 150;
	// 	// console.log(bannerHeight + "- 150 = "+showBtH);
	// 	// 該元素只有適口1856px出現,如果抓不到,判斷式不會觸發
	// 	var eleHeight = $(".indele-pro05").offset().top;
	// 	var winHeight = $(window).height();
	// 	// console.log(eleHeight);
	// 	// console.log(winHeight);
	// 	var hidBtH = eleHeight - winHeight + 100;
	// 	// console.log("卷軸關閉高度"+hidBtH);
	// 	// console.log($(window).scrollTop());
	// 	if($(window).scrollTop()>= showBtH && $(window).scrollTop()<= hidBtH){
	// 		$(".jsindthumbArea").css("display","block");
	// 		TweenMax.staggerTo(".jsthumb", 0, {transform: "translateY(0px)", opacity:1, delay:0}, 0.1);
	// 	}else{
	// 		// console.log("關閉");
	// 		TweenMax.to(".jsthumb", 0.5, {transform: "translateY(-50px)", opacity:0, delay:0}, 0);
	// 	}
	// });


});

// window.onload會等網頁的全部內容，包括圖片，CSS及<iframe>等外部內容載入後才會觸發，但$(document).ready()在Document Object Model (DOM) 載入後就會觸發，所以順序上$(document).ready()會比window.onload先執行。
//window.onload是JavaScript的原生事件，而$(document).ready()是jQuery的事件（其實是透過監聽JavaScript的DOMContentLoaded事件來實現）。
$(window).on('load',function(){
	// $(".js-patLoadingAniBg").addClass("indloadingHide");
	TweenMax.staggerTo(".js-patLoadingAni", 0.5, { width:"25%", opacity: 1, delay: 0.1, ease: Power4.inOut, }, 0.1);
    $(".js-patLoadingAni").addClass("indloadingHide-block");
    $(".js-patLoadingAniBg").addClass("indloadingHide");
	TweenMax.to(".js-patLoadingAniBg", 0, { delay: 1.3, zIndex: -9999});
    // console.log("網頁全部元素載完了");
});


